from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required
from api.api.schemas import SiteSchema
from api.models import Site
from api.commons.pagination import paginate

class SiteList(Resource):
    """Creation and get_all
    ---
    get:
      parameters:
        - in: query
          name: admin_area
          type: string
          required: false
      tags:
        - api
      responses:
        200:
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/PaginatedResult'
                  - type: object
                    properties:
                      results:
                        type: array
                        items:
                          $ref: '#/components/schemas/SiteSchema'
    """

    method_decorators = [jwt_required()]

    def get(self):
        args = request.args
        schema = SiteSchema(many=True)
        if 'area_admin' in args:
            try:
                query = Site.query.filter_by(admin_area=args['area_admin'])
                return paginate(query, schema)
            except Exception as e:
                return str(e)
        else:
            try:
                query = Site.query
                return paginate(query, schema)
            except Exception as e:
                return str(e)
