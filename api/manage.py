import datetime

import click
import pycountry
import requests
from flask.cli import with_appcontext
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from api.extensions import db
from api.models import Site


@click.group()
def cli():
    """Main entry point"""


@cli.command("init")
@with_appcontext
def init():
    """Create a new admin user"""
    from api.extensions import db
    from api.models import User

    click.echo("create user")
    user = User(username="admin", email="admin@mail.com", password="admin", active=True)
    db.session.add(user)
    db.session.commit()
    click.echo("created user admin")


@cli.command("load_sites")
@with_appcontext
def load_sites():
    click.echo("Load sites started!")

    # Loading county codes for alpha_3 standard
    country_codes_alpha3 = {}
    for country in pycountry.countries:
        country_codes_alpha3[country.alpha_2] = country.alpha_3

    country_codes_alpha3['UK'] = 'GBR'
    geoboandaries_dict = get_geoboundaries()

    cb_api_url = "http://api.citybik.es/v2/networks"
    cb_data = requests.get(cb_api_url)

    locations_dict_list = []
    for network in cb_data.json()["networks"]:
        location_dict = {}
        location_dict['city'] = network['location']['city']
        location_dict['country'] = network['location']['country']
        location_dict['latitude'] = network['location']['latitude']
        location_dict['longitude'] = network['location']['longitude']
        location_dict['href'] = network['href']
        location_dict['id'] = network['id']
        location_dict['name'] = network['name']
        location_dict['company'] = network['company']
        location_dict['admin_area'] = ''

        loc_point = Point(location_dict["longitude"], location_dict["latitude"])
        if country_codes_alpha3[location_dict['country']] in geoboandaries_dict.keys():
            for geoboundary in geoboandaries_dict[country_codes_alpha3[location_dict['country']]]:
                polygon = geoboundary["area_shape"]
                if polygon.contains(loc_point):
                    location_dict["admin_area"] = geoboundary["admin_area"]
                    break
        locations_dict_list.append(location_dict)
    for row in locations_dict_list:
        site = Site(
            company=str(row["company"]),
            href=row["href"],
            cycle_id=row["id"],
            city=row["city"],
            country=row["country"],
            latitude=row["latitude"],
            longitude=row["longitude"],
            name=row["name"],
            admin_area=row["admin_area"]
        )
        db.session.add(site)
        db.session.commit()
    click.echo("Load sites finished")
    return


def get_geoboundaries():
    click.echo("Start get boundaries" + datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    geoboundaries = requests.get("https://www.geoboundaries.org/gbRequest.html?ISO=ALL&ADM=ALL")
    geoboundaries_dict = {}
    geoboundaries_json = geoboundaries.json()
    for geoboundary in geoboundaries_json:
        geoboundary_content = requests.get(geoboundary["gjDownloadURL"]).json()
        for feature in geoboundary_content["features"]:
            admin_area = feature["properties"]["shapeID"]
            area_shape = Polygon(feature["geometry"]["coordinates"][0][0])
            if geoboundary['boundaryISO'] not in geoboundaries_dict.keys():
                geoboundaries_dict[geoboundary['boundaryISO']] = []
            geoboundaries_dict[geoboundary['boundaryISO']].append({
                "admin_area": admin_area,
                "area_shape": area_shape
            })
    click.echo("End get boundaries" + datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    return geoboundaries_dict


if __name__ == "__main__":
    cli()
