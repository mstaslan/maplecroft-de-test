from api.extensions import db


class Site(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=True)
    latitude = db.Column(db.Float, nullable=True)
    longitude = db.Column(db.Float, nullable=True)
    admin_area = db.Column(db.String(40), unique=True, nullable=True)
    company = db.Column(db.String(100), nullable=True)
    href = db.Column(db.String(100), nullable=True)
    cycle_id = db.Column(db.String(50), unique=True, nullable=True)
    city = db.Column(db.String(50), nullable=True)
    country = db.Column(db.String(50), nullable=True)